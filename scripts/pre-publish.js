const fs = require("fs-extra");
const path = require("path");

const folders = fs.readdirSync(path.join(".", "dist"));
folders.forEach((folder) => {
  const folderPath = path.join(".", folder);
  if (!fs.existsSync(folderPath)) {
    fs.mkdirSync(folderPath);
  } else {
    fs.emptyDirSync(folderPath);
  }
  fs.copySync(path.join(".", "dist", folder), folderPath, {
    recursive: true,
    overwrite: true,
  });
});
