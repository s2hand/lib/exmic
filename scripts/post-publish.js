const fs = require("fs-extra");
const path = require("path");
const rimraf = require("rimraf");

const folders = fs.readdirSync(path.join(".", "dist"));
folders.forEach((folder) => {
  const folderPath = path.join(".", folder);
  if (fs.existsSync(folderPath)) {
    rimraf(folderPath, () => {
      console.log("Copied built deleted");
    });
  }
});
