import express from "express";
import LogHelper from "../helpers/log.helper";
import { AppError, ManagedError } from "../models/error.model";
import { ResponsePayload } from "../models/response.model";

const handleAppError = (
  error: unknown,
  req: express.Request,
  res: express.Response,
  next: express.NextFunction,
): void => {
  let origin = "UNKNOWN";
  let finalError = error;
  if (error instanceof ManagedError) {
    origin = error.origin;
    finalError = error.error;
  }
  LogHelper.error(origin, finalError);

  let responseHttpCode = 500;
  let responseErrorCode = 9000;
  let responseErrorMessage = "Server Error.";
  if (finalError instanceof AppError) {
    responseHttpCode = finalError.httpCode;
    responseErrorCode = finalError.code;
    responseErrorMessage = finalError.message;
  }
  res
    .status(responseHttpCode)
    .json(new ResponsePayload(responseErrorCode, responseErrorMessage));
  next();
};

export { handleAppError };
