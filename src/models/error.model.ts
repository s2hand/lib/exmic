class AppError {
  public message: string;
  public httpCode: number;
  public code = 9000;

  constructor(message: string, httpCode = 500) {
    this.message = message;
    this.httpCode = httpCode;
  }
}

class ManagedError {
  public origin: string;
  public error: unknown;

  constructor(origin: string, error: unknown) {
    this.origin = origin;
    this.error = error;
  }
}

export { AppError, ManagedError };
