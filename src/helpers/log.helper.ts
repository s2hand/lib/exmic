class LogHelper {
  public static info(
    origin: string,
    message: unknown,
    ...extras: string[]
  ): void {
    const time = new Date().toISOString();
    console.info(
      time,
      `[${LogType.INFO}]`,
      `[${origin}]`,
      ":",
      message,
      ...extras
    );
  }

  public static error(
    origin: string,
    message: unknown,
    ...extras: string[]
  ): void {
    const time = new Date().toISOString();
    console.error(
      time,
      `[${LogType.ERROR}]`,
      `[${origin}]`,
      ":",
      message,
      ...extras
    );
  }
}

enum LogType {
  INFO = "INFO",
  ERROR = "ERROR",
}

export default LogHelper;
